#!/usr/bin/env python3
# encoding: utf-8

import sys
import os

record_filename = "RECORDS.txt"

def main():

    contents = []

    with open(record_filename, mode='r') as record_file:
        for record in record_file:
            with open(record.strip() + '.hea', mode = 'r') as infile:
                for line in infile:
                    contents.append(line.split())
            contents[0] = contents[0][:3]
            with open(record.strip() + '.hea', mode = 'w') as infile:
                for line in contents:
                    print(" ".join(line), file=infile)
            print("processing record {}".format(record.strip()), file=sys.stderr)
            contents = []


if __name__ == '__main__':
    main()

