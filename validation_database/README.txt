This database contains ECG data from the Aera. 

.ann - TZ Medical beat annotations
.atr - WFDB annotations generated from .ann files.
.qrs - XML files containing bead annotations
.hea - Header files with the sample count and subsequent fields removed to bypass the checksum check.
.hea- - Header files with every field intact. These cannot be processed by the WFDB library due to an incorrect checksum.
.dat - Data files containing ECG information.

