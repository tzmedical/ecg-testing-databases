# README #

This repository contains several ECG databases used by TZ Medical for testing arrhythmia analysis algorithms.

Each database contains both ecg data and annotations by a physician to be used as a standard to evaluate algorithms against.  
The data is in wfdb format, with header files (.hea) and data files (.dat).  
A description of the format can be found [here](http://www.physionet.org/physiotools/wag/) (under the WFDB file formats heading).  

Beat level annotations are contained in .atr files, a format which is also outlined in the link above.  
Rhythm level annotations are contained in the files Rhythm_Annotation_atr.txt for each database. The   
first line of the file is a header of the form "Record Type Start Stop Frequency". Each line below  
this denotes a single episode of a type of anomaly.  
Record - The record or file that the episode occurred in.  
Type -  T - Tachycardia, B - Bradycardia, P - Pause, A - Atrial Fibrillation
Start - Start time in number of samples from the beginning of the record 
Stop - End time in number of samples from the beginning of the record  
Frequency - The sample rate, so the above sample counts can be converted back to time.

### Here are the databases contained in this repository. ###

* The MITDB is formally known as the Massachusetts Institute of Technology-Beth Israel Hospital Arrhythmia Database (48 records, 30 minutes each). This database focuses on arrhythmias of various types.  

* The ESCDB, or The European Society of Cardiology ST-T Database, contains 90 records that are 2 hours apiece. These records are concerned with changes to the ST and T waves of a beat.  

* The NSTDB (The Noise Stress Test Database) was also produced by the Massachusetts Institute of Technology-Beth Israel Hospital and is a smaller database that only contains 12 records of 30 minutes each. Rather than focusing on a typical type of beat, this database contains ‘clean’ ECG records that have ‘noise’ artificially added to the ECG records. This check the effectiveness in noise filtration of an algorithm.  

* The CUDB (formally, the Creighton University Sustained Ventricular Arrhythmia Database) contains 35 short records at 8 minutes each. As the name implies, this database focuses on arrhythmias that are sustained through several beats, which means that there are longer periods of time where it may be nearly impossible to detect a beat at all.  

* The validation_database contains data from TZ Medical's Aera device, in addition to hand-corrected annotations for the data it contains.